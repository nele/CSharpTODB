﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpToDBTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string connStr = "Server=121.42.136.4;database=dbtest;User ID=sa;Password=koala19920716!@#";

            //查询的第一种方式
            /*SqlConnection conn = new SqlConnection(connStr);
            SqlCommand cmd = new SqlCommand("select * from t_user",conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while(dr.Read())
            {
                string str = dr.GetSqlString(1).ToString();
                Console.WriteLine(str);
            }
            conn.Close();
            Console.ReadKey();*/

            //查询的第二种方式
            /*SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            SqlCommand cmd = new SqlCommand("select * from t_user", conn);
            DataSet ds = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string str = row["Username"].ToString();
                Console.WriteLine(str);
            }
            conn.Close();
            Console.ReadKey();*/

            ///插入一条数据
            /*SqlConnection conn = new SqlConnection(connStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = "insert into t_user values('test',12,'哈尔滨')";
            conn.Open();
            int result = cmd.ExecuteNonQuery();  //返回影响的行数
            conn.Close();
            Console.WriteLine(result);
            Console.ReadKey();*/

            //删除一条数据
            /* SqlConnection conn = new SqlConnection(connStr);
             SqlCommand cmd = new SqlCommand();
             cmd.Connection = conn;
             cmd.CommandText = "delete from t_user where ID=5";
             conn.Open();
             int result = cmd.ExecuteNonQuery();  //返回影响的行数
             conn.Close();
             Console.WriteLine(result);
             Console.ReadKey();*/

            //修改一条数据
            /* SqlConnection conn = new SqlConnection(connStr);
             SqlCommand cmd = new SqlCommand();
             cmd.Connection = conn;
             cmd.CommandText = "update t_user set Age=10 where ID=1";
             conn.Open();
             int result = cmd.ExecuteNonQuery();  //返回影响的行数
             conn.Close();
             Console.WriteLine(result);
             Console.ReadKey();*/


        }
    }
}
