﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpTODB
{
    class Program
    {
        static void Main(string[] args)
        {
            //1。导入驱动

            //2.链接字符串  创建链接
            string connStr = "Server=121.42.136.4;database=dbtest;User ID=sa;Password=koala19920716!@#";
            SqlConnection conn = new SqlConnection(connStr);

            //3.打开数据库连接
            conn.Open();

            //4.向数据库发送操作指令
            SqlCommand cmd = new SqlCommand("select * from t_user", conn);

            //5.接收结果
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                //得到用户名
                string str = dr.GetString(1);
                Console.WriteLine(str);
            }
            //6.关闭数据库连接
            conn.Close();

            Console.ReadKey();

        }
    }
}
