﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpD
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.导入数据库驱动 sqlserver 直接新建项目的时候驱动已经自动导入 别的数据库需要添加驱动。

            //2.创建连接
            string connStr = "Server=121.42.136.4;database=dbtest;User ID=sa;Password=koala19920716!@#";
            SqlConnection conn = new SqlConnection(connStr);

            //3.打开数据库连接
            conn.Open();

            //4.向数据库发送操作指令
            SqlCommand cmd = new SqlCommand("SELECT * from t_user", conn);

            //5.接收执行的结果
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                string str = dr.GetString(1);
                Console.WriteLine(str);
            }

            //6.关闭数据库连接
            conn.Close();
            Console.ReadKey();

        }
    }
}
